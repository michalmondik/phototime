$(document).ready(function() {

	// owl carousel reviews
	$(".owl-carousel").owlCarousel({
		items:1,
		nav: true,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		dots: false,
		lazyLoad: true,
		autoplay: true,
		loop: true
	});

	// hover effect onto a form while input contains some value
	$('#email').blur(function()
	{
		if( $(this).val().length !== 0 ) {
			$(this).addClass('active-input');
		}
	});

	// SMOOTH SCROLLING
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});

	// sticky header
	var AnimatedHeader = (function() {

		var docElem = document.documentElement,
			header = document.querySelector( '.menu-wrapper' ),
			didScroll = false,
			changeHeaderOn = 100;

		function init() {
			window.addEventListener( 'scroll', function( event ) {
				if( !didScroll ) {
					didScroll = true;
					setTimeout( scrollPage, 250 );
				}
			}, false );
		}

		function scrollPage() {
			var sy = scrollY();
			if ($(window).width() > 768) {
				if (sy >= changeHeaderOn) {
					$('.menu-wrapper').addClass('menu-wrapper-shrink');
				}
				else {
					$('.menu-wrapper').removeClass('menu-wrapper-shrink');
				}
			}
			didScroll = false;
		}

		function scrollY() {
			return window.pageYOffset || docElem.scrollTop;
		}

		init();

	})();
});
